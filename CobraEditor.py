# COBRA GUI text editor 
# Author: Joseph Cox



from Tkinter import *
from tkFileDialog import *

filename = None

def newFile():
    global filename
    filename = "untitled"
    text.delete(0.0,END)
    
def saveFile():
    global filename
    t=text.get(0.0,END)
    f= open(filename,'w')
    f.write(t)
    f.close()
    
def saveAs():
    global filename
    f = asksaveasfile(mode= 'w',defaultextension='.txt')
    t = text.get(0.0,END)
    try:
        f.write(t.rstrip())
    except:
        showerror(title ="ERROR", message="Unable to save the file")
        
def openFile():
    f=askopenfile(mode='r')
    t=f.read()
    text.delete(0.0,END)
    text.insert(0.0,t)
    
root =Tk()
root.title("Cobra Editor")
root.minsize(width=400,height=400)
root.maxsize(width=400,height = 400)

text = Text(root,width=400,height=400)
text.pack()

# Guts of the Menubar
menubar = Menu(root)
fileMenu = Menu(menubar)
fileMenu.add_command(label="New",command = newFile)
fileMenu.add_command(label="open",command = openFile)
fileMenu.add_command(label="save",command = saveFile)
fileMenu.add_command(label="Save as",command = saveAs)
fileMenu.add_separator()
fileMenu.add_command(label="Quit",command = root.quit)
menubar.add_cascade(label="file",menu=fileMenu)
root.config(menu = menubar)
root.mainloop()

    

    
    
    
    

